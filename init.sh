#!/usr/bin/env bash


auth="-u user -p test"

# MONGODB USER CREATION
(
echo "setup mongodb auth"
create_user="if ("'!'"db.getUser('user')) { db.createUser({ user: 'user', pwd: 'test', roles: [ {role:'readWrite', db:'iosr'} ]}) }"
until mongo iosr --eval "$create_user" || mongo iosr $auth --eval "$create_user"; do sleep 5; done
killall mongod
sleep 1
killall -9 mongod
) &


exec gosu mongodb mongod --auth "$@"
