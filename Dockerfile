FROM mongo:3
MAINTAINER Mateusz Jarosz

ADD init.sh /init.sh


RUN chmod +x /init.sh
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install psmisc -y -q
RUN apt-get autoremove -y && apt-get clean
 

ENTRYPOINT ["/init.sh"]
